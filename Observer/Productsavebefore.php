<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Pricelow\Commission\Helper\Data;
use Pricelow\Commission\Setup\Patch\Data\CommissionPricePatch;

/**
 * Class Productsavebefore
 * @package Pricelow\Commission\Observer
 */
class Productsavebefore implements ObserverInterface
{

    /**
     * @var Data
     */
    protected $helper;

    /**
     * Productsavebefore constructor.
     * @param Data $helper
     */
    public function __construct
    (
        Data $helper
    )
    {
        $this->helper = $helper;
    }

    /**
     * @inheritDoc
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        if ($this->helper->isCommissionEnable()) {

            if ($this->helper->getCategoryCommissionData()) {
                $collectionFees = json_decode($this->helper->getCategoryCommissionData(), true);
                $ruleCollection = [];
                $commissionPercentage = [];

                foreach ($collectionFees as $collectionFee) {
                    $ruleCollection[] = $collectionFee;
                }

                $finalPrice = null;
                if ($product->getSpecialPrice()) {
                    $finalPrice = preg_replace('/[^\d. ]/', '', $product->getSpecialPrice());
                } else if($product->getPrice()) {
                    $finalPrice = preg_replace('/[^\d. ]/', '', $product->getPrice());
                }

                if ($finalPrice) {
                    if ($product->getCategoryIds()) {
                        $i = 1;
                        $ruleCategory = null;
                        $html = '';
                        foreach ($product->getCategoryIds() as $productCategory) {
                            foreach ($ruleCollection as $collectionRule => $collectionFee) {
                                if ($productCategory == $collectionFee['category']) {

                                    if ($i == 1) {
                                        $ruleCategory = $collectionFee['category'];
                                    }

                                    if ($ruleCategory == $collectionFee['category']) {
                                        if (($collectionFee['from_price'] <= $finalPrice)) {
                                            $html .= "From " .$collectionFee['from_price'] . " To "  . $collectionFee['to_price'] . " price will applied ". $collectionFee['percentage'] ."% \n";
                                            $commissionPercentage[] = (($finalPrice / 100) * $collectionFee['percentage']);
                                        }
                                        $i++;
                                    }
                                }
                            }
                        }
                    }
                }

                if ($commissionPercentage) {
                    $html .= "Commission applied: ". number_format(array_sum($commissionPercentage), 2);
                    $product->setData(CommissionPricePatch::COMMISSION_PRICE, $html);
                }
            }
        } else {
            $product->setData(CommissionPricePatch::COMMISSION_PRICE, null);
        }
        //echo '<pre>'; print_r($product->debug()); echo '</pre>'; die(__METHOD__.__LINE__);

    }
}
