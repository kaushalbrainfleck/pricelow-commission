<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Helper;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Customer\Model\SessionFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\Pricing\Helper\Data as PriceHelper;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Locale\FormatInterface;

/**
 * Class Data
 * @package Pricelow\Commission\Helper
 */
class Data extends AbstractHelper
{

    const XML_PATH_GENERAL_ENABLE_COMMISSION = "pricelow_commission/general/enable_commission";
    const XML_PATH_COMMISSION_CATEGORY_RANGES = "pricelow_commission/commission/category_ranges";

    /**
     * @var SessionFactory
     */
    protected $_customerSession;

    /**
     * @var PriceHelper
     */
    protected $priceHelper;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @var FormatInterface
     */
    protected $_localeFormat;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * Data constructor.
     * @param Context $context
     * @param SessionFactory $customerSession
     * @param PriceHelper $priceHelper
     * @param Json $jsonSerializer
     * @param FormatInterface $localeFormat
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct
    (
        Context $context,
        SessionFactory $customerSession,
        PriceHelper $priceHelper,
        Json $jsonSerializer,
        FormatInterface $localeFormat,
        ProductRepositoryInterface $productRepository
    )
    {
        $this->_customerSession = $customerSession;
        $this->priceHelper = $priceHelper;
        $this->jsonSerializer = $jsonSerializer;
        $this->_localeFormat = $localeFormat;
        $this->productRepository = $productRepository;
        parent::__construct($context);
    }

    /**
     * @return Session|null
     */
    public function getCustomerSession()
    {
        $customerSession = $this->_customerSession->create();
        if ($customerSession->isLoggedIn()) {
            return $customerSession;
        }else {
            return null;
        }
    }

    /**
     * @return mixed
     */
    public function isCommissionEnable()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_GENERAL_ENABLE_COMMISSION, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public function getCategoryCommissionData()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_COMMISSION_CATEGORY_RANGES, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $price
     * @return float|string
     */
    public function setFormatPrice($price)
    {
        return $this->priceHelper->currency($price, true, false);
    }

    /**
     * @return bool|string
     */
    public function getConfig()
    {
        return $this->jsonSerializer->serialize([
            'format' => $this->_localeFormat->getPriceFormat()
        ]);
    }

    /**
     * @param $id
     * @return ProductInterface|null
     */
    public function getProductById($id)
    {
        try {
            return $this->productRepository->getById($id);
        } catch (\Exception $exception) {
            return null;
        }
    }

}
