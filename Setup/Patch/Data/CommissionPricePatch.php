<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Setup\Patch\Data;

use Magento\Catalog\Model\Config;
use Magento\Catalog\Model\Product;
use Magento\Eav\Api\AttributeManagementInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;

/**
 * Class CommissionPricePatch
 * @package Pricelow\Commission\Setup\Patch\Data
 */
class CommissionPricePatch implements DataPatchInterface, PatchRevertableInterface
{
    const COMMISSION_PRICE = "commission_price";

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var CategorySetupFactory
     */
    private $categorySetupFactory;

    /**
     * AddHoldAttribute constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param CategorySetupFactory $categorySetupFactory
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        CategorySetupFactory $categorySetupFactory,
        EavSetupFactory $eavSetupFactory
    )
    {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->categorySetupFactory = $categorySetupFactory;

    }

    /**
     * @inheritDoc
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritDoc
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        /** @var EavSetup $eavSetup */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $categorySetup = $this->categorySetupFactory->create(['setup' => $this->moduleDataSetup]);
        $entityTypeId = $categorySetup->getEntityTypeId(Product::ENTITY);
        $diamondAttributeSetId = $eavSetup->getAttributeSet($entityTypeId, 'Default');

        $eavSetup->addAttribute(
            Product::ENTITY,
            self::COMMISSION_PRICE,
            [
                'type' => 'varchar',
                'backend' => '',
                'frontend' => '',
                'label' => 'Commission Price',
                'input' => 'textarea',
                'class' => '',
                'source' => '',
                'global' => ScopedAttributeInterface::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'readonly' => true,
                'user_defined' => true,
                'searchable' => false,
                'filterable' => true,
                'comparable' => false,
                'visible_on_front' => true,
                'used_in_product_listing' => true,
                'unique' => false
            ]
        );
        $eavSetup->addAttributeToSet(
            $entityTypeId,
            $diamondAttributeSetId['attribute_set_id'],
            "Product Details",
            self::COMMISSION_PRICE,
            40
        );

        $this->setupAttribute();

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @inheritDoc
     */
    public function revert()
    {
        return [];
    }

    /**
     * @return $this
     */
    public function setupAttribute()
    {
        $objectManager = ObjectManager::getInstance();
        $state = $objectManager->get('Magento\Framework\App\State');
        $state->setAreaCode('adminhtml');

        $ATTRIBUTE_CODE = self::COMMISSION_PRICE;
        $ATTRIBUTE_GROUP = 'Product Details';
        /* Attribute assign logic */
        $eavSetup = $objectManager->create(EavSetup::class);
        $config = $objectManager->get(Config::class);
        $attributeManagement = $objectManager->get(AttributeManagementInterface::class);

        $entityTypeId = $eavSetup->getEntityTypeId(Product::ENTITY);
        $attributeSetIds = $eavSetup->getAllAttributeSetIds($entityTypeId);
        foreach ($attributeSetIds as $attributeSetId) {
            if ($attributeSetId) {
                $group_id = $config->getAttributeGroupId($attributeSetId, $ATTRIBUTE_GROUP);
                $attributeManagement->assign(
                    'catalog_product',
                    $attributeSetId,
                    $group_id,
                    $ATTRIBUTE_CODE,
                    40
                );
            }
        }
        return $this;
    }
}
