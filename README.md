# Pricelow Commission for Magento 2

Pricelow-Commission module functionality is represented by the following sub-systems:
 - Todo.....

## How to install & upgrade Brainfleck_Conditionalwidget

### 1. Copy and paste

- Download latest zip for Pricelow-Commission
- Extract zip file to `app/code/Pricelow/Commission` ; You should create a folder path `app/code/Pricelow/Commission` if not exist.
- Go to Magento root folder and run upgrade command line to install `Pricelow_Commission`:

```
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy
```

#v1.0.0
Released on 2021-07-31

Release notes:
```
Compatible with Magento [2.3.6 To 2.4.X]
```
