<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Block\Adminhtml\Form\Field;

use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Context;
use Magento\Framework\View\Element\Html\Select;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class CategoryColumn
 * @package Pricelow\Commission\Block\Adminhtml\Form\Field
 */
class CategoryColumn extends Select
{

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollection;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * TaxColumn constructor.
     * @param CategoryCollectionFactory $categoryCollection
     * @param StoreManagerInterface $storeManager
     * @param Context $context
     * @param array $data
     */
    public function __construct
    (
        CategoryCollectionFactory $categoryCollection,
        StoreManagerInterface $storeManager,
        Context $context,
        array $data = []
    )
    {
        $this->categoryCollection = $categoryCollection;
        $this->_storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * @param $value
     * @return CategoryColumn
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * @return string
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        return parent::_toHtml();
    }


    /**
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    private function getSourceOptions()
    {
        $categories = $this->categoryCollection();
        $categoryArr = [];
        foreach ($categories as $category) {
            $categoryArr[] = ['label' => $category->getName(), 'value' => $category->getId()];
        }
        unset($categoryArr[0]);
        unset($categoryArr[1]);
        return $categoryArr;
    }

    /**
     * @return Collection
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    protected function categoryCollection()
    {
        return $this->categoryCollection->create()
            ->addAttributeToSelect('*')
            ->setStore($this->_storeManager->getStore());
    }
}
