<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\Component\Form;
use Pricelow\Commission\Setup\Patch\Data\CommissionPricePatch;

/**
 * Class BundlePanel
 * @package Pricelow\Commission\Ui\DataProvider\Product\Form\Modifier
 */
class BundlePanel
{
    /**
     * @param \Magento\Bundle\Ui\DataProvider\Product\Form\Modifier\BundlePanel $subject
     * @param $meta
     * @return array
     */
    public function afterModifyMeta(
        \Magento\Bundle\Ui\DataProvider\Product\Form\Modifier\BundlePanel $subject,
        $meta
    ) {
        $meta['bundle-items']['children']['bundle_options']['children']['record']['children']['product_bundle_container']['children']['bundle_selections']['children']['record']['children'][CommissionPricePatch::COMMISSION_PRICE] = $this->getCommissionData();
        return $meta;
    }

    /**
     * Get selection custom field structure
     *
     * @return array
     */
    protected function getCommissionData()
    {
        return [
            'arguments' => [
                'data' => [
                    'config' => [
                        'componentType' => Form\Field::NAME,
                        'dataType' => 'varchar',
                        'formElement' => 'textarea',
                        'elementTmpl' => 'ui/dynamic-rows/cells/text',
                        'label' => __('Commission Price'),
                        'dataScope' => CommissionPricePatch::COMMISSION_PRICE,
                        'sortOrder' => 80,
                    ],
                ],
            ],
        ];
    }
}
