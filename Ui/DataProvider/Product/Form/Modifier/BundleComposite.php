<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Ui\DataProvider\Product\Form\Modifier;

use Magento\Bundle\Model\Product\Type;
use Magento\Bundle\Ui\DataProvider\Product\Form\Modifier\BundlePanel;
use Magento\Bundle\Ui\DataProvider\Product\Form\Modifier\Composite;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\NoSuchEntityException;
use Pricelow\Commission\Setup\Patch\Data\CommissionPricePatch;

/**
 * Class BundleComposite
 * @package Pricelow\Commission\Ui\DataProvider\Product\Form\Modifier
 */
class BundleComposite extends Composite
{

    /**
     * @param array $data
     * @return array
     * @throws InputException
     * @throws NoSuchEntityException
     */
    public function modifyData(array $data)
    {

        /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
        $product = $this->locator->getProduct();
        $modelId = $product->getId();
        $isBundleProduct = $product->getTypeId() === Type::TYPE_CODE;

        if ($isBundleProduct && $modelId) {

            $data[$modelId][BundlePanel::CODE_BUNDLE_OPTIONS][BundlePanel::CODE_BUNDLE_OPTIONS] = [];

            /** @var \Magento\Bundle\Api\Data\OptionInterface $option */

            foreach ($this->optionsRepository->getList($product->getSku()) as $option) {

                $selections = [];

                foreach ($option->getProductLinks() as $productLink) {


                    $linkedProduct = $this->productRepository->get($productLink->getSku());
                    $integerQty = 1;
                    if ($linkedProduct->getExtensionAttributes()->getStockItem()) {
                        if ($linkedProduct->getExtensionAttributes()->getStockItem()->getIsQtyDecimal()) {
                            $integerQty = 0;
                        }
                    }
                    $selections[] = [
                        'selection_id' => $productLink->getId(),
                        'option_id' => $productLink->getOptionId(),
                        'product_id' => $linkedProduct->getId(),
                        'name' => $linkedProduct->getName(),
                        'sku' => $linkedProduct->getSku(),
                        CommissionPricePatch::COMMISSION_PRICE => $this->getCommissionPrice($linkedProduct->getId()) ? $this->getCommissionPrice($linkedProduct->getId()) : null,
                        'is_default' => ($productLink->getIsDefault()) ? '1' : '0',
                        'selection_price_value' => $productLink->getPrice(),
                        'selection_price_type' => $productLink->getPriceType(),
                        'selection_qty' => (bool)$integerQty ? (int)$productLink->getQty() : $productLink->getQty(),
                        'selection_can_change_qty' => $productLink->getCanChangeQuantity(),
                        'selection_qty_is_integer' => (bool)$integerQty,
                        'position' => $productLink->getPosition(),
                        'delete' => '',
                    ];
                }

                $data[$modelId][BundlePanel::CODE_BUNDLE_OPTIONS][BundlePanel::CODE_BUNDLE_OPTIONS][] = [
                    'position' => $option->getPosition(),
                    'option_id' => $option->getOptionId(),
                    'title' => $option->getTitle(),
                    'default_title' => $option->getDefaultTitle(),
                    'type' => $option->getType(),
                    'required' => ($option->getRequired()) ? '1' : '0',
                    'bundle_selections' => $selections,
                ];
            }

        }

        return $data;
    }

    /**
     * @param $productId
     * @return float|null
     */
    protected function getCommissionPrice($productId)
    {
        $commissionPrice = null;
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $currency = $objectManager->get('Magento\Framework\Pricing\Helper\Data');
        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
        if($product->getData('commission_price')) {
            $commissionPrice = $product->getData('commission_price');
        }

        return $commissionPrice;
    }

}
