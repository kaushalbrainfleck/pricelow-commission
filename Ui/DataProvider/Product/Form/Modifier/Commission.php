<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Ui\DataProvider\Product\Form\Modifier;

use Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AbstractModifier;
use Magento\Ui\DataProvider\Modifier\ModifierInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Pricelow\Commission\Setup\Patch\Data\CommissionPricePatch;
use Pricelow\Commission\Helper\Data as PricelowHelperData;

/**
 * Class Commission
 * @package Pricelow\Commission\Ui\DataProvider\Product\Form\Modifier
 */
class Commission extends AbstractModifier implements ModifierInterface
{

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var PricelowHelperData
     */
    protected $pricelowHelperData;

    /**
     * @var array
     */
    protected $meta = [];

    /**
     * Commission constructor.
     * @param ArrayManager $arrayManager
     * @param PricelowHelperData $pricelowHelperData
     */
    public function __construct(
        ArrayManager $arrayManager,
        PricelowHelperData $pricelowHelperData
    )
    {
        $this->arrayManager = $arrayManager;
        $this->pricelowHelperData = $pricelowHelperData;
    }

    /**
     * {@inheritdoc}
     *
     * @param array $meta
     * @return array
     */
    public function modifyMeta(array $meta)
    {
        if ($this->arrayManager->findPath(CommissionPricePatch::COMMISSION_PRICE, $meta, null, 'children')) {
            $commissionPath = $this->arrayManager->findPath(CommissionPricePatch::COMMISSION_PRICE, $meta, null, 'children');
            $meta = $this->arrayManager->set(
                "{$commissionPath}/arguments/data/config/disabled",
                $meta,
                true
            );
            $meta = $this->arrayManager->set(
                "{$commissionPath}/arguments/data/config/dataType",
                $meta,
                'textarea'
            );
            $meta = $this->arrayManager->set(
                "{$commissionPath}/arguments/data/config/formElement",
                $meta,
                'textarea'
            );
            $meta = $this->arrayManager->set(
                "{$commissionPath}/arguments/data/config/sortOrder",
                $meta,
                35
            );

            $commissionArgsPath = 'product-details/children/container_commission_price';
            $meta = $this->arrayManager->set(
                "{$commissionArgsPath}/arguments/data/config/sortOrder",
                $meta,
                35
            );

            $meta = $this->arrayManager->remove(
                "{$commissionPath}/arguments/data/config/validation",
                $meta
            );

            $meta = $this->arrayManager->remove(
                "{$commissionPath}/arguments/data/config/addbefore",
                $meta
            );
        }

        if ($this->arrayManager->findPath("price", $meta, null, 'children')) {
            $pricePath = $this->arrayManager->findPath("price", $meta, null, 'children');
            $meta = $this->arrayManager->set(
                "{$pricePath}/arguments/data/config/commissionData",
                $meta,
                $this->getCommissionData()
            );
        }

        if ($this->arrayManager->findPath("special_price", $meta, null, 'children')) {
            $specialPricePath = $this->arrayManager->findPath("special_price", $meta, null, 'children');
            $meta = $this->arrayManager->set(
                "{$specialPricePath}/arguments/data/config/commissionData",
                $meta,
                $this->getCommissionData()
            );
        }

        if ($this->arrayManager->findPath("category_ids", $meta, null, 'children')) {
            $categoriesPath = $this->arrayManager->findPath("category_ids", $meta, null, 'children');
            $meta = $this->arrayManager->set(
                "{$categoriesPath}/arguments/data/config/commissionData",
                $meta,
                $this->getCommissionData()
            );
        }
        //echo '<pre>'; print_r($meta); echo '</pre>'; die(__METHOD__.__LINE__);
        return $meta;
    }

    /**
     * {@inheritdoc}
     */
    public function modifyData(array $data)
    {
        return $data;
    }

    /**
     * @return \array[][][][]
     */
    protected function getFields()
    {
        return [
            CommissionPricePatch::COMMISSION_PRICE => [
                'arguments' => [
                    'data' => [
                        'config' => [
                            'dataType' => 'textarea',
                            'formElement' => 'textarea',
                            'visible' => 1,
                            'required' => 0,
                            'notice' => null,
                            'default' => null,
                            'label' => __('Commission Price'),
                            'code' => CommissionPricePatch::COMMISSION_PRICE,
                            'source' => 'product-details',
                            'scopeLabel' => '[GLOBAL]',
                            'globalScope' => null,
                            'sortOrder' => 40,
                            'componentType' => 'field'
                        ]
                    ]
                ]
            ]

        ];
    }

    /**
     * @return array
     */
    protected function getCommissionData()
    {
        $ruleCollection = null;

        if($this->pricelowHelperData->getCategoryCommissionData()) {
            $collectionFees = json_decode($this->pricelowHelperData->getCategoryCommissionData(), true);
            foreach ($collectionFees as $collectionFee) {
                $ruleCollection[] = $collectionFee;
            }
        }

        return $ruleCollection;
    }
}
