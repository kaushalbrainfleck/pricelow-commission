<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Plugin\CheckoutModel;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Pricelow\Commission\Helper\Data as PriceLowHelper;
use Magento\Checkout\Model\DefaultConfigProvider;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Pricelow\Commission\Setup\Patch\Data\CommissionPricePatch;

class DefaultConfigProviderExtended
{

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var PriceLowHelper
     */
    protected $priceLowHelper;

    /**
     * @var PricingHelper
     */
    protected $pricingHelper;


    /**
     * Constructor
     *
     * @param CheckoutSession $checkoutSession
     * @param priceLowHelper $priceLowHelper
     * @param PricingHelper $pricingHelper
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        priceLowHelper $priceLowHelper,
        PricingHelper $pricingHelper
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->priceLowHelper = $priceLowHelper;
        $this->pricingHelper = $pricingHelper;
    }

    /**
     * @param DefaultConfigProvider $subject
     * @param array $result
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterGetConfig(DefaultConfigProvider $subject, array $result)
    {
        if ($this->priceLowHelper->isCommissionEnable()) {
            $items = $result['totalsData']['items'];

            foreach ($items as $index => $item)
            {
                $totalCommission = null;
                $quoteItem = $this->checkoutSession->getQuote()->getItemById($item['item_id']);
                if ($quoteItem['qty_options']) {
                    foreach ($quoteItem['qty_options'] as $option) {
                        $commissionPrice = $this->getCommissionData($option['product_id']);
                        if ($commissionPrice) {
                            $totalCommission += $commissionPrice;
                        }
                    }
                }
                if ($totalCommission) {
                    $result['totalsData']['items'][$index]['bundle_commission'] = $totalCommission ? $this->pricingHelper->currency(round($totalCommission, 2), true, false) : null;
                }
            }
        }

        return $result;
    }

    /**
     * @param $id
     * @return float
     */
    protected function getCommissionData($id)
    {
        try {
            $product = $this->priceLowHelper->getProductById($id);
            if ($product->getData(CommissionPricePatch::COMMISSION_PRICE)) {
                return $product->getData(CommissionPricePatch::COMMISSION_PRICE);
            } else {
                return null;
            }
        } catch (\Exception $exception) {
            echo $exception->getTraceAsString();
            return null;
        }
    }

}
