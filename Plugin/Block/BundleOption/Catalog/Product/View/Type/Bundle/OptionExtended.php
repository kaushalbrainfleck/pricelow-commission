<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Plugin\Block\BundleOption\Catalog\Product\View\Type\Bundle;

use Pricelow\Commission\Setup\Patch\Data\CommissionPricePatch;
use Magento\Bundle\Block\Catalog\Product\View\Type\Bundle\Option;
use Magento\Catalog\Model\Product;
use Magento\Framework\Pricing\Helper\Data as PricingHelper;
use Pricelow\Commission\Helper\Data as PriceLowHelper;

/**
 * Class OptionExtended
 * @package Pricelow\Commission\Plugin\Block\BundleOption\Catalog\Product\View\Type\Bundle
 */
class OptionExtended
{
    /**
     * @var PricingHelper
     */
    protected $pricingHelper;

    /**
     * @var PriceLowHelper
     */
    protected $priceLowHelper;

    /**
     * OptionExtended constructor.
     * @param PricingHelper $pricingHelper
     * @param PriceLowHelper $priceLowHelper
     */
    public function __construct(PricingHelper $pricingHelper, PriceLowHelper $priceLowHelper)
    {
        $this->pricingHelper = $pricingHelper;
        $this->priceLowHelper = $priceLowHelper;
    }

    /**
     * @param Option $subject
     * @param $result
     * @param Product $selection
     * @return string
     */
    public function afterGetSelectionTitlePrice(Option $subject, $result, Product $selection)
    {
        if ($this->priceLowHelper->isCommissionEnable()) {
            if ($selection->getData(CommissionPricePatch::COMMISSION_PRICE)) {
                $result .= '<br><div class="commission-wrap">
                                <span style="font-size: 15px;"><strong>'. __("Commission Fee: ") .'</strong><span class="commission-label">'.$this->pricingHelper->currency(round($selection->getData(CommissionPricePatch::COMMISSION_PRICE), 2),true, false).'</span></span>
                            </div>';
            }
        }
        return $result;
    }
}
