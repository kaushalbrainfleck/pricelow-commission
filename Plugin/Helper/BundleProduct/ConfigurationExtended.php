<?php
/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

namespace Pricelow\Commission\Plugin\Helper\BundleProduct;

use Magento\Bundle\Helper\Catalog\Product\Configuration;
use Magento\Catalog\Model\Product\Configuration\Item\ItemInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Pricing\Helper\Data;
use Pricelow\Commission\Helper\Data as PriceLowHelper;
use Pricelow\Commission\Setup\Patch\Data\CommissionPricePatch;
use Magento\Framework\Escaper;

/**
 * Class ConfigurationExtended
 * @package Pricelow\Commission\Plugin\Helper\BundleProduct
 */
class ConfigurationExtended
{
    /**
     * Core data
     *
     * @var Data
     */
    protected $pricingHelper;

    /**
     * @var PriceLowHelper
     */
    protected $priceLowHelper;

    /**
     * Escaper
     *
     * @var Escaper
     */
    protected $escaper;

    /**
     * Serializer interface instance.
     *
     * @var Json
     */
    private $serializer;

    /**
     * ConfigurationExtended constructor.
     * @param Data $pricingHelper
     * @param PriceLowHelper $priceLowHelper
     * @param Escaper $escaper
     * @param Json|null $serializer
     */
    public function __construct(
        Data $pricingHelper,
        PriceLowHelper $priceLowHelper,
        Escaper $escaper,
        Json $serializer = null
    ) {
        $this->priceLowHelper = $priceLowHelper;
        $this->pricingHelper = $pricingHelper;
        $this->escaper = $escaper;
        $this->serializer = $serializer ?: ObjectManager::getInstance()
            ->get(Json::class);
    }

    /**
     * @param Configuration $subject
     * @param callable $proceed
     * @param ItemInterface $item
     * @return array
     */
    public function aroundGetBundleOptions(Configuration $subject, callable $proceed, ItemInterface $item)
    {
        $options = [];
        $product = $item->getProduct();

        /** @var \Magento\Bundle\Model\Product\Type $typeInstance */
        $typeInstance = $product->getTypeInstance();

        // get bundle options
        $optionsQuoteItemOption = $item->getOptionByCode('bundle_option_ids');
        $bundleOptionsIds = $optionsQuoteItemOption
            ? $this->serializer->unserialize($optionsQuoteItemOption->getValue())
            : [];

        if ($bundleOptionsIds) {
            $optionsCollection = $typeInstance->getOptionsByIds($bundleOptionsIds, $product);

            // get and add bundle selections collection
            $selectionsQuoteItemOption = $item->getOptionByCode('bundle_selection_ids');

            $bundleSelectionIds = $this->serializer->unserialize($selectionsQuoteItemOption->getValue());

            if (!empty($bundleSelectionIds)) {
                $selectionsCollection = $typeInstance->getSelectionsByIds($bundleSelectionIds, $product);

                $bundleOptions = $optionsCollection->appendSelections($selectionsCollection, true);
                foreach ($bundleOptions as $bundleOption) {
                    if ($bundleOption->getSelections()) {
                        $option = ['label' => $bundleOption->getTitle(), 'value' => []];

                        $bundleSelections = $bundleOption->getSelections();

                        foreach ($bundleSelections as $bundleSelection) {
                            $qty = $subject->getSelectionQty($product, $bundleSelection->getSelectionId()) * 1;
                            if ($qty) {
                                $option['value'][] = $qty . ' x '
                                    . $this->escaper->escapeHtml($bundleSelection->getName())
                                    . ' '
                                    . $this->pricingHelper->currency(
                                        $subject->getSelectionFinalPrice($item, $bundleSelection)
                                    );
                                $option['has_html'] = true;
                            }

                            $bundleOptionId = $bundleSelection->getProductId();
                            if ($this->getProductCommission($bundleOptionId)) {
                                $option[CommissionPricePatch::COMMISSION_PRICE] = $this->getProductCommission($bundleOptionId) ? $this->getProductCommission($bundleOptionId) : null;
                            }
                        }

                        if ($option['value']) {
                            $options[] = $option;
                        }
                    }
                }
            }
        }

        return $options;
    }

    /**
     * @param $optionId
     * @return null
     */
    protected function getProductCommission($optionId)
    {
        try {
            $product = $this->priceLowHelper->getProductById($optionId);
            if ($product->getData(CommissionPricePatch::COMMISSION_PRICE)) {
                return $this->pricingHelper->currency(round($product->getData(CommissionPricePatch::COMMISSION_PRICE), 2), true, false);
            } else {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }

    }
}
