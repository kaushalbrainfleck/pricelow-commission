/**
 * Pricelow
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the brainfleck.com license that is
 * available through the world-wide-web at this URL:
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Pricelow
 * @package     Pricelow_Commission
 * @copyright   Copyright (c) Pricelow (http://brainfleck.com/)
 */

var config = {
    map: {
        '*': {
            'Magento_Ui/js/form/element/abstract': 'Pricelow_Commission/js/form/element/abstract',
            'Magento_Ui/js/form/element/ui-select': 'Pricelow_Commission/js/form/element/ui-select'
        }
    }
}
